import { ITodo } from '@/models/ITodo'
import Axios from 'axios'
import {Getters, Mutations, Actions, Module, createMapper} from 'vuex-smart-module'

class RootState {
    todos: ITodo[] = [
        {id: 1, title: 'todo1', completed: false},
        {id: 2, title: 'todo2', completed: true},
        {id: 3, title: 'todo3', completed: false},
    ]

    loadingStatus: boolean = false
}

class RootGetters extends Getters<RootState> {
    get allTodos() {
        return this.state.todos
    }

    get loadingStatus() {
        return this.state.loadingStatus
    }
}


class RootMutations extends Mutations<RootState> {
    deleteTodo(id: number) {
        this.state.todos = this.state.todos.filter(todo => todo.id !== id)
    }

    setLoadingStatus(status: boolean) {
        this.state.loadingStatus = status
    }

    addTodo(todo: ITodo) {
        this.state.todos.push(todo)
    }

    checkTodo(id: number) {
        this.state.todos.map(todo => {
            if (todo.id === id) {
                todo.completed = !todo.completed
            }
        })
    }

    loadTodos(todos) {
        this.state.todos = todos
    }
    
}

class RootActions extends Actions<
    RootState,
    RootGetters,
    RootMutations,
    RootActions
> {
    deleteTodo(payload: number) {
        this.commit('deleteTodo', payload)
    }

    addTodo(payload: ITodo) {
        this.commit('addTodo', payload)
    }

    checkTodo(payload: number) {
        this.commit('checkTodo', payload)
    }

    loadTodos() {
        this.commit('setLoadingStatus', true)
        fetch('https://jsonplaceholder.typicode.com/todos')
            .then(res => res.json())
            .then(result => {
                this.commit('loadTodos', result)
                this.commit('setLoadingStatus', false)
            })
    }

}

export const root = new Module({
    state: RootState,
    getters: RootGetters,
    mutations: RootMutations,
    actions: RootActions
})

export const rootMapper = createMapper(root)